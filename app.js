const express = require('express');
const app = express();
require('dotenv').config();
require('express-async-errors');
const {
    bindRoutes
} = require('./routes.js');

bindRoutes(app);
app.listen(process.env.PORT, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log(`app is listening on port ${process.env.PORT}`);
    }
});