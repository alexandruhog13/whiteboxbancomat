const bancomatRoutes = require('./controllers/bancomat.js');

module.exports.bindRoutes = (app) => {
    app.use('/bancomat', bancomatRoutes);
    app.use((err, req, res, next) => {
        if (err.message === 'Invalid Operation') {
            res.status(400);
        }
        res.json({
            error: err.message
        });
        next(err);
    })
}