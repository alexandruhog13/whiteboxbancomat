# WhiteboxBancomat

Cum sa rulati:

1.  docker-compose pull
2.  docker-compose up

Programul prezinta 2 rute HTTP:

*  GET http://localhost:3000/bancomat/stock -> informatii despre stock
*  GET http://localhost:3000/bancomat/:valoare_numerica -> impartire in bancnote

Timp efectiv de lucru: 2 ore