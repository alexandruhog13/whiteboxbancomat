const router = require('express').Router();
const {
    getStock,
    setStock
} = require('../stoc.js');
const MailClient = require('../services/mail.js');

const tryToPay = (amountObj, stock, bill) => {
    let nr = Math.floor(amountObj.val / bill);
    if (nr !== 0) {
        if (nr > stock[bill]) {
            nr = stock[bill];
        }
        stock[bill] -= nr;
        amountObj.val -= nr * bill;
        return nr;
    }
    return 0;
}


router.get('/stock', (req, res) => {
    res.json(getStock());
})

router.get('/:amount', async (req, res) => {
    const {
        amount,
    } = req.params;

    const bills = {
        100: 0,
        50: 0,
        10: 0,
        5: 0,
        1: 0
    };

    const amountNumber = parseInt(amount, 10);
    const dummyStock = getStock();

    if (isNaN(amountNumber)) {
        throw Error('Invalid Operation');
    }

    const amountObj = {
        val: amountNumber,
    };

    bills[100] = tryToPay(amountObj, dummyStock, 100);
    if (amountObj.val != 0) {
        bills[50] = tryToPay(amountObj, dummyStock, 50);
    }
    if (amountObj.val != 0) {
        bills[10] = tryToPay(amountObj, dummyStock, 10);
    }

    if (amountObj.val != 0) {
        bills[5] = tryToPay(amountObj, dummyStock, 5);
    }

    if (amountObj.val != 0) {
        bills[1] = tryToPay(amountObj, dummyStock, 1);
    }

    if (amountObj.val != 0) {
        res.json({
            message: 'We are sorry but your amount could not been retrieved'
        });
    } else {
        if (dummyStock[100] !== 0 && dummyStock[100] <= dummyStock.criticalLimit100 * dummyStock.initialDeposit100 / 100) {
            //send email critical
            const mailClient = new MailClient()
                .setTo('fillMeUpPlease@superbancomat.com')
                .setSubject('[CRITICAL 100] - Bancomat')
                .setText(`Your 100 bills stock has fallen bellow ${dummyStock.criticalLimit100}% and is now ${dummyStock[100]}`);
            console.log(await mailClient.sendMail());
            console.log('100 - critical');
        } else {
            if (dummyStock[100] !== 0 && dummyStock[100] <= dummyStock.warningLimit100 * dummyStock.initialDeposit100 / 100) {
                //send email warning
                const mailClient = new MailClient()
                    .setTo('fillMeUpPlease@superbancomat.com')
                    .setSubject('[WARNING 100] - Bancomat')
                    .setText(`Your 100 bills stock has fallen bellow ${dummyStock.warningLimit100}% and is now ${dummyStock[100]}`);
                console.log(await mailClient.sendMail());
                console.log('100 - warning');
            }
        }
        if (dummyStock[50] !== 0 && dummyStock[50] < dummyStock.warningLimit50 * dummyStock.initialDeposit50 / 100) {
            //send email warning
            const mailClient = new MailClient()
                .setTo('fillMeUpPlease@superbancomat.com')
                .setSubject('[WARNING 50] - Bancomat')
                .setText(`Your 50 bills stock has fallen bellow ${dummyStock.warningLimit50}% and is now ${dummyStock[50]}`);
            console.log(await mailClient.sendMail());
            console.log('50 - warning');
        }
        setStock(dummyStock);
        res.json(bills);
    }
});

module.exports = router;