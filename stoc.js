const stock = {
    1: 100,
    5: 100,
    10: 100,
    50: 50,
    100: 50,
    warningLimit100: 20,
    criticalLimit100: 10,
    warningLimit50: 15,
    initialDeposit100: 50,
    initialDeposit50: 50,
}
module.exports.getStock = () => stock;
module.exports.setStock = dummyStock => {
    stock[1] = dummyStock[1];
    stock[5] = dummyStock[5];
    stock[10] = dummyStock[10];
    stock[50] = dummyStock[50];
    stock[100] = dummyStock[100];
}