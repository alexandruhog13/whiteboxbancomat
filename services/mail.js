const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
    host: process.env.EMAILHOST,
    port: process.env.EMAILPORT,
    auth: {
        user: process.env.EMAILUSER,
        pass: process.env.EMAILPASSWORD
    }
});

class Mail {
    constructor() {
        this.from = process.env.EMAILUSER;
        this.to = null;
        this.suject = null;
        this.text = null;
        this.transport = transport;
    }

    setTo(to) {
        this.to = to;
        return this;
    }

    setSubject(subject) {
        this.subject = subject;
        return this;
    }

    setText(text) {
        this.text = text;
        return this;
    }

    setHtml(html) {
        this.html = html;
        return this;
    }
    async sendMailWithHtml() {
        return await this.transport.sendMail({
            from: this.from,
            to: this.to,
            subject: this.subject,
            html: this.html
        });
    }
    async sendMail() {
        return await this.transport.sendMail({
            from: this.from,
            to: this.to,
            subject: this.subject,
            text: this.text
        });
    }
}

module.exports = Mail;